# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

require 'securerandom' 

Dado(/^que eu queira fazer uma venda do "([^"]*)" Tim$/) do |plano|
 	
 	@plano              = plano	
 	@guuid              = SecureRandom.uuid
 	@msisdn             = "55109#{Faker::Number.number(8)}"
end

Quando(/^forem passados os parametros corretamente com o "([^"]*)"$/) do |service_code|
	
	 @service_code            = service_code
  	 @createbody_hubtim       = {
  
	  		"external_uuid" => @guuid,
	  		"gateway_postfields"=> 
	  		{
	    	"msisdn"=> @msisdn,
	    	"subscriptionId"=> @msisdn,
	    	"token"=> ""
	  		},
	  		"order_info"=> {
	    	"user_ref"=> 10,
	    	"product_ref"=> 20,
	    	"service"=> @plano,
	    	"origin"=> 0
	  		},
	  		"payment_date"=> Time.now.strftime("%Y-%m-%d"),
	  		"payment_type"=> 2,
	  		"service_code"=> @service_code,
	  		"gratuity"=> 0
			}.to_json

	puts "\nBODY PASSADO\n"
	puts @createbody_hubtim

  	endpoint               = $api['billinghubtim']
  	mount                  = endpoint.gsub("<guid>", @guuid)

 	@venda_tim = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'njzlmdrjymrmntjly2jkmjaznwe0mmy0odlkngrhy'}, :body => @createbody_hubtim)
 	puts (mount)

end

Então("a venda sera feita com sucesso") do

  	puts "\nRESULTADO DO TESTE\n"
 	puts @venda_tim.body
  	
  		case @venda_tim.code
    		when 200
      	puts "Retornou 200, ok"
    		when 404
      	puts "Retornou 404, não existe"
    		when 400
      	puts "Retornou 400, problema de negócio"
    		when 500...600
      	puts "ops #{@venda_tim.code}"
  	end

  	expect(@venda_tim.code).to eq 202
  

end

E("receberei o retorno do banco de dados com o id") do
	
	@resultado = billing_db
	puts @resultado
	
    
 end

 Dado("que eu tenha vendas aguardando retorno do datasync") do

   hubtim_status   		
   	
end

Quando("o datasync rodar") do

    aguardando_billing
end

Então("as vendas receberão o retorno de sucesso na cobrança") do
 
   callbackdatasync

end


Dado("que eu tenha vendas aguardando retorno datasync") do

  listacancelartim

end

Quando("for enviado um cancelamento") do

  cancelartim
  
end


Então("as vendas serão canceladas no billing") do

  listancanceladostim

end

Dado("que eu possua vendas aguardando retorno do datasync") do
  
  hubtim_status 

end

Quando("o datasync sincronizar") do

 aguardando_billing

end

Então("as vendas receberao o retorno na cobrança de erro") do

 callbackerros

 end
