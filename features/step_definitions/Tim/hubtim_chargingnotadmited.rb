# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

require 'securerandom' 


Dado("que eu tenha vendas aguardando retorno.p") do
  
	@listaID = []
    status = aguardando_billing 
    puts "Total de vendas aguardando retorno: #{status.count}"

end

Quando("rodar o datasync.p") do

  puts $api['datasynctim']

	resultadouuid = data_sync
	resultadouuid.to_a.each do | lista |
		@txId           = SecureRandom.uuid
		@uuid           = lista['uuid']
		@idCliente      = lista['id']
		@nextBillDate   = Time.now + (60*60*24)
		@datasync       = { 
		  "ChargingNotificationRequest":{  
		    "txId"=> @txId ,
		    "serviceId"=> "TESTE_QA",
		    "chargingTxId"=> @uuid,
		    "result" =>  "ERR_CHARGING_NOT_ADMITTED",
		    "nextBillDate"=> @nextBillDate.strftime("%d/%m/%Y 03:00:00 +0000")
		  }
		}.to_json

		@listaID.push(@idCliente) 
		@datasynctim = HTTParty.post($api['datasynctim'],:headers => {"Content-Type" => 'application/json', "Authorization" => 'njzlmdrjymrmntjly2jkmjaznwe0mmy0odlkngrhy'}, :body => @datasync)
        puts datasync

		case @datasynctim.code
		when 200
			puts "Retornou 200, ok"
		when 404
		  	puts "Retornou 404, não existe"
		when 400
		  	puts "Retornou 400, problema de negócio"
		when 500...600
		  	puts "ops #{@datasynctim.code}"
		end

		expect(@datasynctim.code).to eq 200

	end 

end


Então("as vendas receberão o retorno de charging not admited") do

  @listaID.to_a.each do | id |
		valida = valida_cancelamento(id)
		valida.each do | row |
			case row["total"]
			when 1
				puts "Order #{id} cancelado com sucesso"
			else
			  	puts "Erro, order #{id} nao cancelado"
			end
			expect(row["total"]).to eq 1
			
		end
	end
end