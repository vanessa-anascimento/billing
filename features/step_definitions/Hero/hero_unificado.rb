# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)


require 'securerandom' 


Dado(/^que eu queira fazer uma venda do "([^"]*)" Hero$/) do |plano|

puts "\nInicio Contratacao\n"

end

Quando(/^eu enviar a "([^"]*)" para o 5513$/) do |kw|
  @kw = kw

  contratar_hero

end

Então("receberei um sms de confirmacao") do

  contratar_hero_confirme

end

Então("o plano será contratado") do
  
  puts "\nConsulta Produto Contratado\n"
  consultar_produto_ativo

end

Dado("que eu tenha o Hero assinado") do
  
  contratar_heroup
  contratar_hero_confirme
  consultar_produto_ativo

end

Quando("eu enviar uma KW contratando um plano superior ao assinado") do

  contratar_upgrade
  contratar_hero_confirme

end

Então("o upgrade deverá ser feito com sucesso") do

  consultar_produto_ativo

end 

Dado("que eu tenha um plano Hero assinado") do
  
  contratar_heroup
  contratar_hero_confirme
  consultar_produto_ativo

end

Quando("eu enviar uma KW contratando um plano inferior ao assinado") do

  contratar_downgrade
  contratar_hero_confirme

end

Então("o downgrade deverá ser feito com sucesso") do

  consultar_produto_ativo

end

Dado("que eu tenha o hero assinado") do
  contratar_heroup
  contratar_hero_confirme

  consultar_produto_ativo

end

Quando("for solicitado um cancelamento para o hero") do
  
  cancelar_hero

end

Então("o cancelamento do hero sera efetuado com sucesso") do

  consultar_produto_inativo

end





