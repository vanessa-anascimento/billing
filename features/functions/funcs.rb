def cancelartim()

	resultadohash = []
	resultadohash = hashcancelar

	resultadohash.to_a.each do | lista |
		@guid				= SecureRandom.uuid
		@hash               = lista

		endpoint            = $api['billingcancel']
		mount               = endpoint.gsub("<guid>", @guid)
		mount               = mount.gsub("<hashcancel>", @hash)

		#monta endpoint
		#puts mount

		@canceltim = HTTParty.delete(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'njzlmdrjymrmntjly2jkmjaznwe0mmy0odlkngrhy'})
		puts "Hash para ser cancelado:" + lista
		puts "Retorno TIM #{@canceltim}"
			
		case @canceltim.code
			when 200
				puts "Retornou 200, ok"
			when 404
				puts "Retornou 404, cliente não existe"
			when 400
				puts "Retornou 400, dados enviados estão errados"
			when 500...600
				puts "ops - erro interno #{@canceltim.code}"
		end

		expect(@canceltim.code).to eq 200
	end
end


def callbackdatasync()

	resultadouuid = []
 		 resultadouuid = data_sync

 		 resultadouuid.to_a.each do | lista |

			@txId           = SecureRandom.uuid
			@uuid           = lista
			@datasync       = {  
		   
		   		"ChargingNotificationRequest":{  
		      	"txId"=> @txId ,
		      	"serviceId"=> "TESTE_QA",
		      	"chargingTxId"=> @uuid['uuid'],
		    	"result" =>  "ERR_NOT_CREDIT",
		      	"nextBillDate"=> Time.now.strftime("%Y-%m-%d")
		   		}
				}.to_json

			puts "\nBODY ENVIADOO\n"
			puts @datasync

		 	@datasynctim = HTTParty.post($api['datasynctim'],:headers => {"Content-Type" => 'application/json', "Authorization" => 'njzlmdrjymrmntjly2jkmjaznwe0mmy0odlkngrhy'}, :body => @datasync)
			 puts ($api['datasynctim'])

		 	puts "\nIMPRIMINDO RESULTADO DO TESTE\n"
		 	puts @datasynctim.body
		  		
		  		case @datasynctim.code
		    		when 200
		      	puts "Retornou 200, ok"
		    		when 404
		      	puts "Retornou 404, não existe"
		    		when 400
		      	puts "Retornou 400, problema de negócio"
		    		when 500...600
		      	puts "ops #{@datasynctim.code}"
		  	end

		  	expect(@datasynctim.code).to eq 200

		end 
end


def contratar_hero()

  @msisdns                    = "5540#{Faker::Number.number(9)}"
  @transaction_id             = Faker::Number.number(21)
  @auth_code                  = Faker::Number.number(4)
  @transaction_id_auth        = Faker::Number.number(21)

  @createbody_hero            = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => @kw,
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_hero)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_hero.code).to eq 200

  sleep 3
  

end


def contratar_hero_confirme()

   @createbody_sim             = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => "sim",
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_sim 
    
  @create_sim = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_sim)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_sim.code).to eq 200

end


def consultar_produto_ativo()

	sleep 2

	endpoint             = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @msisdns)

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n"
  puts (mount)

  puts "\nIMPRIMINDO RESULTADO DA CONSULTA\n"
 	puts @consulta_produto_ativo.body
  	case @consulta_produto_ativo.code
    	when 200
        puts "Retornou 200, ok"
    	when 404
        puts "Retornou 404, não existe"
    	when 400
        puts "Retornou 400, problema de negócio"
    	when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
  	end

  expect(@consulta_produto_ativo.code).to eq 200
  get = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)
  expect(get.success).to eq true

end

def contratar_heroup()

  @msisdns                    = "5540#{Faker::Number.number(9)}"
  @transaction_id             = Faker::Number.number(21)
  @auth_code                  = Faker::Number.number(4)
  @transaction_id_auth        = Faker::Number.number(21)

  @createbody_hero            = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => 7,
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_hero)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_hero.code).to eq 200

end

def contratar_upgrade()

   @createbody_hero            = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => 5,
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_hero)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_hero.code).to eq 200

end

def contratar_downgrade()
 
  @createbody_hero            = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => 5,
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_hero)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_hero.code).to eq 200

end

def cancelar_hero()
 
  @createbody_hero            = {
    "msisdn"                  => @msisdns,
    "la"                      => 5513,
    "text"                    => "sair",
    "partner"                 => "claro",
    "debug"                   => true,
    "transaction_id"          => @transaction_id,
    "auth_code"               => @auth_code,
    "auth_code_status"        =>  true,
    "transaction_id_auth"     => @transaction_id_auth
}.to_json
  
  puts @createbody_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_hero)
  puts ($api["\ncontratar_hero\n"])

  expect(@create_hero.code).to eq 200

end

def consultar_produto_inativo()

	sleep 2

	endpoint             = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @msisdns)

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n"
  puts (mount)

  puts "\nIMPRIMINDO RESULTADO DA CONSULTA\n"
 	puts @consulta_produto_ativo.body
  	case @consulta_produto_ativo.code
    	when 200
        puts "Retornou 200, ok"
    	when 404
        puts "Retornou 404, não existe"
    	when 400
        puts "Retornou 400, problema de negócio"
    	when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
  	end

  expect(@consulta_produto_ativo.code).to eq 404
  get = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)
  expect(get.success).to eq false

end

def callbackerros()

  erros = ["ERR_CHARGING_NOT_ADMITTED", "ERR_BLACKLISTED_CUSTOMER", "ERR_TEMPORARY_ERROR", "ERR_UNKNOWN_ERROR", "ERR_REASON_NOT_ADMITTED"]

  resultadouuid = data_sync

  resultadouuid.to_a.each do | lista |
    nextBillDate = Time.now
    erro = erros.sample
    puts "\n\n\n\n\n\n"
    puts erro
    puts "\n"
    
    if erro == "ERR_CHARGING_NOT_ADMITTED" then
      nextBillDate = Time.now + (60*60*24)
    end

    puts nextBillDate
    puts "\n\n\n\n\n\n"
    @txId           = SecureRandom.uuid
    @uuid           = lista
    @datasync       = {  

      "ChargingNotificationRequest":{  
        "txId"=> @txId ,
        "serviceId"=> "TESTE_QA",
        "chargingTxId"=> @uuid['uuid'],
        "result" =>  erro,
        "nextBillDate"=> nextBillDate.strftime("%Y-%m-%d")
      }
    }.to_json

    puts "\nBODY ENVIADOO\n"
    puts @datasync
    puts "\nPARA API\n"
    puts $api['datasynctim']

    @datasynctim = HTTParty.post($api['datasynctim'],:headers => {"Content-Type" => 'application/json', "Authorization" => 'njzlmdrjymrmntjly2jkmjaznwe0mmy0odlkngrhy'}, :body => @datasync)

    puts "\nIMPRIMINDO RESULTADO DO TESTE\n"
    puts @datasynctim.body

    case @datasynctim.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@datasynctim.code}"
    end

    expect(@datasynctim.code).to eq 200

  end 

end
