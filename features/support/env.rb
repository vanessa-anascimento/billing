# This file provides setup and common functionality across all features.  It's
# included first before every test run, and the methods provided here can be 
# used in any of the step definitions used in a test.  This is a great place to
# put shared data like the location of your app, the capabilities you want to
# test with, and the setup of selenium.

require "rspec/expectations"
require "cucumber"
require "pry"
require "rspec"
require "httparty"
require "pry"
require "faker"
require "cpf_faker"
require "mysql2"
require "net/https"


$profile = ENV['PROFILE']
api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]

