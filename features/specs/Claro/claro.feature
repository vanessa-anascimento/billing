# language: pt

Funcionalidade: Envio de vendas para o Billing Claro.

    @vendaclaro
	Esquema do Cenario: Fluxo de envio de vendas para o Billing Claro, de todos os produtos comercializados.
	
	Dado que eu queira fazer uma venda do <plano> Claro
	Quando forem passados os parametros corretamente com <servicecode>
	Então a venda sera feita 	
	E receberei o retorno do banco com o id

	Exemplos:
    |plano                                 |servicecode|
	|"77000 - Interatividades"			   |"100001"   |
	|"77000 - Vídeo Download I - Parceiro" |"000002"   |
	|"77000 - Download Interativo"         |"000003"   |
	|"77000 - Quiz - Assinatura I"         |"000004"   |
	|"77000 - Download Interativo"         |"000005"   |
	|"77000 - Quiz - Assinatura I"         |"000006"   |
	|"77000 - Download Interativo"         |"000007"   |
	|"77000 - Download Interativo"         |"000008"   |
	|"22020 - Interatividades"             |"000009"   |
	|"22020  - Vídeo Download I - Parceiro"|"000010"   |
	|"22020 - Download Interativo"         |"000011"   |
	|"22020 - Quiz - Assinatura I"         |"000012"   |
	|"22020 - Download Interativo"         |"000013"   |
	|"22020 - Quiz - Assinatura I"         |"000014"   |
	|"22020 - Download Interativo"         |"000015"   |
	|"22020 - Download Interativo"         |"000016"   |
	|"Ensina Conteúdo - Semanal"           |"000017"   |
	|"Ensina Conteúdo - Degrau 2"          |"000018"   |
	|"Ensina Conteúdo - Degrau 1"          |"000019"   |
	|"Ensina Conteúdo - Mensal"            |"000021"   |
	|"Ensina Conteúdo - Degrau 1"          |"000022"   |
	|"Ensina Conteúdo - Degrau 2"          |"000023"   |
	|"Ensina Conteúdo - Degrau 3"          |"000024"   |
	|"Ensina Conteúdo - Degrau 5"          |"000026"   |
	|"Promo - Games Full"                  |"000027"   |
	|"Promo - Famosos Full"                |"000028"   |
	|"Promo - Games Degrau"                |"000029"   |
	|"Hero-Promo"                          |"000030"   |
	|"Hero-Promo"                          |"000031"   |
	|"Hero-Promo"                          |"000032"   |
	|"Ensina ConteÃºdo - Semanal"          |"000033"   |
	|"Ensina Conteúdo - Degrau 4"          |"000035"   |
	|"AJUDA"                               |"000036"   |
	|"BASICO"                              |"000037"   |
	|"ESSENCIAL"                           |"000038"   |
	|"FAMILIA"                             |"000039"   |
	|"HERO AVANCADO LOJA"                  |"000040"   |
	|"HERO BASICO LOJA"                    |"000041"   |
	|"HERO INTERMEDIARIO LOJA"             |"000042"   |
	|"HERO PCT ADICIONAL 150GB LOJA"       |"000043"   |
	|"HERO PCT ADICIONAL 500GB LOJA"       |"000044"   |
	|"HERO PROMO CLARO"                    |"000045"   |
	|"PREMIUM"                             |"000046"   |
	|"SUPER"                               |"000048"   |
	|"TOP"                                 |"000050"   | 
	|"BASICO-2"                            |"000051"   |
	|"ESSENCIAL-2"                         |"000052"   |
	|"PREMIUM-2"                           |"000053"   |
	|"Promo - Famosos Full"                |"000054"   |
	|"Promo - Famosos Full Degrau 2"       |"000055"   |


	