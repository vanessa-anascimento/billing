# language: pt


Funcionalidade: Envio de vendas para o Billing HubTim com retorno do Datasync, simulando cenários de resposta do Global.

    @vendatim
	Esquema do Cenario: Fluxo de envio de vendas para o Billing HubTim, de todos os produtos comercializados
	
	Dado que eu queira fazer uma venda do <plano> Tim
	Quando forem passados os parametros corretamente com o <service_code>
	Então a venda sera feita com sucesso	
	E receberei o retorno do banco de dados com o id
	 
    Exemplos:
    |plano                      |service_code|
    |"Protect Essencial"        |"013559"    |
    |"Protect Premium "         |"013560"    |
    |"Protect Familia"          |"013561"    |
    |"Protect Familia Premium"  |"013562"    |
    |"Protect Backup 100GB"     |"013558"    |
    |"Protect Backup 10GB" 	    |"013556"    |
	  |"Protect Backup 30GB" 	    |"013557"	   |
	  |"Protect Backup 50GB" 	    |"013355"    |
	  |"Protect Backup 5GB"       |"013555"    |
    |"Beleza"                   |"013575"    |
    |"Cantada"                  |"013574"    |
    |"Curioso"                  |"013572"    |
    |"Esporte"                  |"013573"    |
    |"Hero-Promo"               |"013959"    |
    |"Kids Criar"               |"013565"    |
    |"Para Choque Caminhao"     |"013576"    |
    |"Pensamentos"              |"013570"    |
    |"Piadas"                   |"013571"    |
    |"Protect Antirroubo"       |"013564"    |
    |"Protect Seguranca"        |"013554"    |
    |"Protect Seguro Computador"|"013563"    |
    |"TIM Protect Seguranca"    |"013960"    |
	  
     @datasynctimsucesso
  Cenario: Datasync com resposta de sucesso na cobrança

  Dado que eu tenha vendas aguardando retorno do datasync 
  Quando o datasync rodar
  Então as vendas receberão o retorno de sucesso na cobrança 


     @cancelamentobillingtim
  Cenario: Cancelamento de vendas no billing

  Dado que eu tenha vendas aguardando retorno datasync
  Quando for enviado um canc    elamento
  Então as vendas serão canceladas no billing

    @datasyncerros
  Cenario: Datasync com resposta de erros na cobrança

  Dado que eu possua vendas aguardando retorno do datasync 
  Quando o datasync sincronizar
  Então as vendas receberao o retorno na cobrança de erro 

    

