# language: pt

Funcionalidade: Billing unificado Hero.

@vendahero
Esquema do Cenario: Fluxo vendas Hero
	
	Dado que eu queira fazer uma venda do <plano> Hero
	Quando eu enviar a <kw> para o 5513
	Então receberei um sms de confirmacao	
	E o plano será contratado



	Exemplos:
    |plano                                 |kw       |
	|"BASICO"                              |"BASICO" |
	|"PREMIUM"                             |"PREMIUM"|
	|"FAMILIA"                             |"FAMILIA"|
	|"TOP"                                 |"TOP"    |



#@upgrade
#Cenario: Upgrade Hero

	#Dado que eu tenha o Hero assinado
	#Quando eu enviar uma KW contratando um plano superior ao assinado
	#Então o upgrade deverá ser feito com sucesso

#@downgrade
#Cenario: Downgrade Hero

	#Dado que eu tenha um plano Hero assinado
	#Quando eu enviar uma KW contratando um plano inferior ao assinado
	#Então o downgrade deverá ser feito com sucesso


@cancelarhero
Cenario: Cancelar Hero

 	Dado que eu tenha o hero assinado
  	Quando for solicitado um cancelamento para o hero
	Então o cancelamento do hero sera efetuado com sucesso
	

